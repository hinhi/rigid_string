#include <stdlib.h>
/* #include <stdio.h> */
/* #include <math.h> */

/* #define F_Y(y, a, s) ((a) / sqrt(1.0+(a)*(a))) */
/* #define F_A(y, a, s) (((a) - k*(y)) / (1.0 - (s))) */


/* double euler(int DN, double k, double a) { */
/*     double y = 0.0; */
/*     double fy = 0.0; */
/*     double ds = 1.0 / DN; */
    
/*     for (int it=0; it < DN; it++) { */
/*         double dy = F_Y(y,a,ds*it) * ds; */
/*         double da = F_A(y,a,ds*it) * ds; */
/*         y += dy; */
/*         a += da; */
/*         fy += y; */
/*     } */
/*     return fy * ds; */
/* } */

/* double euler_simp(int DN, double k, double a) { */
/*     double y = 0.0; */
/*     double fy = 0.0; */
/*     double ds = 1.0 / DN; */
    
/*     for (int it=0; it < DN; it++) { */
/*         double dy = F_Y(y,a,ds*it) * ds; */
/*         double da = F_A(y,a,ds*it) * ds; */
/*         y += dy; */
/*         a += da; */
/*         fy += y * (1 << (2-(it&1))); */
/*     } */
/*     fy -= y; */
/*     return fy * ds / 3; */
/* } */

/* double euler2(int DN, double k, double a) { */
/*     double y = 0.0; */
/*     double fy = 0.0; */
/*     double ds = 1.0 / DN; */
    
/*     for (int it=0; it < DN; it++) { */
/*         y += a * ds / sqrt(1.0 + a*a); */
/*         a += (a - k*y) * ds / (1.0 - ds*(it+0.5)); */
/*         fy += y; */
/*     } */
/*     return fy * ds; */
/* } */

/* double euler2_simp(int DN, double k, double a) { */
/*     double y = 0.0; */
/*     double fy = 0.0; */
/*     double ds = 1.0 / DN; */
    
/*     for (int it=0; it < DN; it++) { */
/*         y += a * ds / sqrt(1.0 + a*a); */
/*         a += (a - k*y) * ds / (1.0 - ds*(it+0.5)); */
/*         fy += y * (1 << (2-(it&1))); */
/*     } */
/*     fy -= y; */
/*     return fy * ds / 3; */
/* } */

/* double rk4(int DN, double k, double a) { */
/*     double y = 0.0; */
/*     double fy = 0.0; */
/*     double ds = 1.0 / DN; */
    
/*     for (int it=0; it < DN-1; it++) { */
/*         double dy1 = F_Y(y, a, ds*it) * ds; */
/*         double da1 = F_A(y, a, ds*it) * ds; */
/*         double dy2 = F_Y(y+dy1*0.5, a+da1*0.5, ds*(it+0.5)) * ds; */
/*         double da2 = F_A(y+dy1*0.5, a+da1*0.5, ds*(it+0.5)) * ds; */
/*         double dy3 = F_Y(y+dy2*0.5, a+da2*0.5, ds*(it+0.5)) * ds; */
/*         double da3 = F_A(y+dy2*0.5, a+da2*0.5, ds*(it+0.5)) * ds; */
/*         double dy4 = F_Y(y+dy3, a+da3, ds*(it+1)) * ds; */
/*         double da4 = F_A(y+dy3, a+da3, ds*(it+1)) * ds; */
/*         y += (dy1 + dy2*2 + dy3*2 + dy4) / 6; */
/*         a += (da1 + da2*2 + da3*2 + da4) / 6; */
/*         fy += y; */
/*     } */
/*     double dy = F_Y(y,a,ds*(DN-1)) * ds; */
/*     double da = F_A(y,a,ds*(DN-1)) * ds; */
/*     y += dy; */
/*     a += da; */
/*     fy += y; */
/*     return fy * ds; */
/* } */

/* double rk4_simp(int DN, double k, double a) { */
/*     double y = 0.0; */
/*     double fy = 0.0; */
/*     double ds = 1.0 / DN; */
    
/*     for (int it=0; it < DN-1; it++) { */
/*         double dy1 = F_Y(y, a, ds*it) * ds; */
/*         double da1 = F_A(y, a, ds*it) * ds; */
/*         double dy2 = F_Y(y+dy1*0.5, a+da1*0.5, ds*(it+0.5)) * ds; */
/*         double da2 = F_A(y+dy1*0.5, a+da1*0.5, ds*(it+0.5)) * ds; */
/*         double dy3 = F_Y(y+dy2*0.5, a+da2*0.5, ds*(it+0.5)) * ds; */
/*         double da3 = F_A(y+dy2*0.5, a+da2*0.5, ds*(it+0.5)) * ds; */
/*         double dy4 = F_Y(y+dy3, a+da3, ds*(it+1)) * ds; */
/*         double da4 = F_A(y+dy3, a+da3, ds*(it+1)) * ds; */
/*         y += (dy1 + dy2*2 + dy3*2 + dy4) / 6; */
/*         a += (da1 + da2*2 + da3*2 + da4) / 6; */
/*         fy += y * (1 << (2-(it&1))); */
/*     } */
/*     double dy = F_Y(y,a,ds*(DN-1)) * ds; */
/*     double da = F_A(y,a,ds*(DN-1)) * ds; */
/*     y += dy; */
/*     a += da; */
    fy += y;
    return fy * ds / 3;
}

double calc_fy(double k, double a) {
    double v0 = euler(1<<14, k, a);
    double v1 = euler(1<<15, k, a);
    double v2 = euler(1<<16, k, a);
    double d = v2 - 2.0*v1 + v0;
    if (d != 0.0) {
        return v0 - (v1-v0)*(v1-v0) / d;
    } else {
        return v0;
    }
}

double calc_e(double k, double a) {
    return k*calc_fy(k, a) - a;
}

double bin_search(double k, double x0, double y0, double x1, double y1) {
    if (y0 > y1) {
        double t;
        // t = y0;
        // y0 = y1;
        // y1 = t;
        t = x0;
        x0 = x1;
        x1 = t;
    }
    while (fabs((x1-x0)/x1) > 1e-10) {
        double m = (x0 + x1) * 0.5;
        if (calc_e(k, m) > 0.0) {
            x1 = m;
        } else {
            x0 = m;
        }
    }
    return x1;
}

void search_a(double k) {
    double pre_a = 1;
    double pre_e = calc_e(k, pre_a);
    double da = 0.95;
    double a = pre_a * da;
    
    printf("%.16e ", k);
    while (a > 1e-12) {
        double e = calc_e(k, a);
        if (e * pre_e <= 0.0) {
            printf("%.16e ", bin_search(k, pre_a, pre_e, a, e));
        }
        pre_e = e;
        pre_a = a;
        a *= da;
    }
    printf("\n");
}

int main(int argc, char **argv) {
    int n = 1024;
    for (int kk = n; kk < 3*n/2; kk++) {
        search_a(kk*1.0/n);
    }
    return 0;
}

// int main(int argc, char const *argv[]) {
//     double k = atof(argv[1]);
//     double a = atof(argv[2]);
//     for (int i=8; i < 26; i++) {
//         printf("%d %.20e %.20e %.20e %.20e %.20e %.20e\n", i, euler(1<<i,k,a), euler2(1<<i,k,a), rk4(1<<i,k,a), euler_simp(1<<i,k,a), euler2_simp(1<<i,k,a), rk4_simp(1<<i,k,a));
//     }
//     return 0;
// }

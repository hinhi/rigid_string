import sys
from math import *


def calc_path(N, k, w):
    ds = 1.0 / N
    x = 0.0
    y = 0.0
    path = [[0.0]*3]
    for i in xrange(N):
        dx = ds / sqrt(1.0 + w*w)
        x += dx
        y += w * dx
        w += (w - k*y) * ds / (1.0-ds*(i+0.5))
        path.append([x, y, i*ds])
    return path

def _calc_phs(path):
    xx = 0.0
    K = 0.0
    max_y = -1.0
    for x,y,s in path:
        xx += x
        y2 = y * y
        K += y2 * 0.5
        if max_y < y2:
            max_y = y2
    ds = 1.0 / (len(path)-1)
    return 0.5-xx*ds, K*ds, sqrt(max_y), path[-1][0]

def calc_phs(k, w):
    U1, K1, y1, x1 = _calc_phs(calc_path(2**12, k, w))
    U2, K2, y2, x2 = _calc_phs(calc_path(2**14, k, w))
    U3, K3, y3, x3 = _calc_phs(calc_path(2**16, k, w))
    return (U1-(U2-U1)**2/(U3-2*U2+U1),
            K1-(K2-K1)**2/(K3-2*K2+K1),
            y1-(y2-y1)**2/(y3-2*y2+y1),
            x1-(x2-x1)**2/(x3-2*x2+x1) )
            
def main():
    iname = sys.argv[1]
    oname = sys.argv[2]
    of = open(oname, "w")
    for line in open(iname):
        line = line.split()
        k = float(line[0])
        of.write("%.5e "%k)
        for w in line[1:]:
            w = float(w)
            U, K, y, x = calc_phs(k, w)
            of.write("%.5e %.5e %.5e %.5e %.5e "%(w, U, K, y, x))
        of.write("\n")
        of.flush()

if __name__ == '__main__':
    main()
